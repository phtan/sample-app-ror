module ApplicationHelper

	def full_title(page_title = '')
		# Returns the full title (as defined in Hartl's tutorial 
		# on Ruby on Rails), on a per-page basis
		base_title = "Sample app from a tutorial on RoR"
		if page_title.empty?
			base_title
		else
			page_title + " | " + base_title
		end
end
end
